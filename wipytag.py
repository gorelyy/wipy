# -*- coding: UTF-8 -*-
import os
import lxml.html
import shutil
import yaml
import wipygit

class Tag:
    def __init__(self, root, tagname, mainconfig):
        self.mainconfig = mainconfig
        self.config = {}
        self.name = tagname
        self.idname = tagname.replace(' ', '_')
        self.root = root
        self.path = root + '/tag/' + self.idname
        self.valid = False
        if not os.path.exists(self.path):
            if '#' in self.name or '.' in self.name:
                print u'Имя ' + self.name + u' содержит неподдерживаемые символы. Создание тега остановлено'
                return
            try:
                os.makedirs(self.path)
            except WindowsError:
                print u'Имя ' + self.name + u' содержит неподдерживаемые символы. Создание тега остановлено'
                return
            template = open(self.root + '/template/tagpage.html', 'r').read()
            root = lxml.html.fromstring(template)
            header = lxml.html.fromstring('<h1>'+self.name+'</h1>')
            pageheader = root.find_class('pageheader')
            pageheader[0].insert(0, header)
            for title in root.iter('title'):
                title.text = self.mainconfig['title']['front'] + self.name \
                    + self.mainconfig['title']['back']
            index = open(self.path + '/index.html', 'w')
            index.write(lxml.html.tostring(root))
            wipygit.gitAdd(self.root, 'tag/' + self.idname + '/index.html')
            open(self.path + '/config.yml', 'w').close()
            self.config['pages'] = []
            self.writeConfig()
        self.readConfig()
        self.valid = True
        
    def readConfig(self):
        config = open(self.path + '/config.yml', 'r')
        self.config = yaml.load(config)
        config.close()

    def writeConfig(self):
        stream = open(self.path + '/config.yml', 'w')
        stream.write(yaml.dump(self.config, default_flow_style=False))

    def generateTagPage(self):
        pages = sorted(self.config['pages'])
        firstletters = set()
        for pagename in pages:
            firstletters.add(pagename[:1].upper())
        
        # Create html with tag as title
        template = open(self.root + '/template/tagpage.html', 'r').read()
        root = lxml.html.fromstring(template)
        header = lxml.html.fromstring('<h1>#'+self.name+'</h1>')
        pageheader = root.find_class('pageheader')
        pageheader[0].insert(0, header)
        for title in root.iter('title'):
            title.text = self.mainconfig['title']['front'] + self.name \
                + self.mainconfig['title']['back']
        for letter in sorted(list(firstletters)):
            header = lxml.html.fromstring(
                '<h3 class="index">' +
                letter +
                '</h3> <ul class="taglist' +
                letter +
                '"></ul>')
            indeces = root.find_class('index')
            i = 0
            if indeces is not None:
                i = len(indeces) + 1
            page = root.find_class(self.mainconfig['tagpagediv'])
            page[0].insert(i, header)
            ul = root.find_class('taglist'+letter)
            for pagename in pages:
                if pagename[:1].upper() == letter:
                    link = pagename
                    if pagename == self.mainconfig['homepage']:
                        link = ''
                    li = lxml.html.fromstring(
                        '<li class="taggedpage' + letter + '">' +
                        '<a href="/' + link.replace(' ', '_') + '">' +
                        pagename + '</a></li>')
                    taggedpages = root.find_class('taggedpage' + letter)
                    i = 0
                    if taggedpages is not None:
                        i = len(taggedpages) + 1
                    ul[0].insert(i, li)
        index = open(self.path + '/index.html', 'w')
        index.write(lxml.html.tostring(root))

    def addPage(self, pagename):
        if pagename not in self.config['pages']:
            self.config['pages'].append(pagename)
            self.writeConfig()
            self.generateTagPage()

    def pageCount(self):
        return len(self.config['pages'])

    def delete(self):
        shutil.rmtree(self.path)

    def removePage(self, pagename):
        if pagename not in self.config['pages']:
            return
        self.config['pages'].remove(pagename)
        if len(self.config['pages']) > 0:
            self.writeConfig()
            self.generateTagPage()
        else:
            self.delete()

    def renamePage(self, pagename, newpagename):
        if pagename not in self.config['pages']:
            return
        self.config['pages'].remove(pagename)
        self.config['pages'].append(newpagename)
        self.writeConfig()
        self.generateTagPage()
