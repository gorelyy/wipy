import sys, os, requests, time, random, hashlib, json, codecs, urllib2, lxml.html, webbrowser
import yaml

class Imagebam:
    def __init__(self, keysfilename):
        self.initialized = False
        self.keysfilename = keysfilename
        keysfile = open(keysfilename, 'r')
        self.keys = yaml.load(keysfile)
        keysfile.close()
        self.dir = os.getcwd()
        self.api_request = {    
            'oauth_consumer_key':self.keys['API_KEY'],
            'oauth_token':self.keys['OAUTH_TOKEN'],
            'oauth_signature_method':'MD5', 
            'oauth_signature':'',
            'oauth_timestamp':'',
            'oauth_nonce':''
        }
        
        self.image_request = {
            'content_type':'family',
            'thumb_format':'JPG',
            'thumb_size':'350x350',
            'thumb_cropping':'0',
            'thumb_info':'1',
            'gallery_id':'',
            'response_format':'JSON'
        }
        
        if self.keys['OAUTH_TOKEN'] == None:
            choice = raw_input('You need to verify images uploading to imagebam. Proceed now? Y/N')
            if choice == 'Y' or choice == 'y':
                result,unauth = self.getUnauthToken()
                self.api_request['oauth_token'] = unauth[0]
                self.keys['OAUTH_TOKEN'] = unauth[0]
                self.keys['OAUTH_TOKEN_SECRET'] = unauth[1]
                if result == False:
                    return
                verifier = self.getVerifier(self.keys['OAUTH_TOKEN'])
                result,access = self.getAccessToken(verifier)
                self.api_request['oauth_token'] = access[0]
                self.keys['OAUTH_TOKEN'] = access[0]
                self.keys['OAUTH_TOKEN_SECRET'] = access[1]
                if result == False:
                    return
                keysfile = open(keysfilename, 'w')
                keysfile.write(yaml.dump(self.keys,default_flow_style=False))
                keysfile.close()
            else:
                return
        self.initialized = True
    def nonce(self):
        random_number = ''.join(str(random.randint(0, 9)) for i in range(40))
        return hashlib.md5(str(time.time()) + str(random_number)).hexdigest()
        
    def updateOAUTH(self):
        oauth_timestamp = int(time.time())
        oauth_nonce = self.nonce()
        oauth_signature = hashlib.md5(self.keys['API_KEY'] 
            + (self.keys['API_SECRET'] or '') 
            + str(oauth_timestamp) 
            + oauth_nonce 
            + (self.keys['OAUTH_TOKEN'] or '')
            + (self.keys['OAUTH_TOKEN_SECRET'] or '')).hexdigest()      
        self.api_request['oauth_signature'] = str(oauth_signature)
        self.api_request['oauth_timestamp'] = str(oauth_timestamp)
        self.api_request['oauth_nonce'] = str(oauth_nonce)

    def getUnauthToken(self):
        self.updateOAUTH()
        unauth_request = self.api_request
        del unauth_request['oauth_token']
        answer = requests.post(self.request_unauth_token_url, params = unauth_request)
        if answer.status_code != 200: 
            print 'getUnauthToken Error ' + str(answer.status_code)
            return False, (0,0)    
        a = answer.text.split('&')
        token = a[0].split('=')[1]
        secret = a[1].split('=')[1]
        return True,(token,secret)

    def getVerifier(self,oauthtoken):
        webbrowser.open_new(self.verifier_url + '?oauth_token=' + oauthtoken)
        verifier = raw_input("Please enter 8 digits verifier string:")
        return verifier[:8]

    def getAccessToken(self,verifier):
        self.updateOAUTH()
        access_request = self.api_request
        access_request['oauth_verifier'] = verifier
        answer = requests.post(self.request_token_url, params = access_request)
        if answer.status_code != 200: 
            print 'getAccessToken Error ' + str(answer.status_code)
            return False, (0,0)
        a = answer.text.split('&')
        token = a[0].split('=')[1]
        secret = a[1].split('=')[1]
        return True,(token,secret)

    def uploadImage(self,filename):
        if self.isInitialized() == False:
            return
        self.updateOAUTH()
        upload_image_request = dict(self.api_request.items() + self.image_request.items())
        image_file = {'image':''}
        image_file['image'] = open(filename, 'rb')
        result = False
        imagepageurl = ''
        requestindex = 0
        while result == False and requestindex < 3:
            new_image = requests.post(self.post_image_url,params=upload_image_request, files=image_file)
            result = new_image.status_code == requests.codes.ok and new_image.json()['rsp']['status'] == 'ok'
            if result == False:
                print('Error: Cant upload ' + filename + '\n' + new_image.text)
                requestindex = requestindex + 1
            else:
                imagepageurl = new_image.json()['rsp']['image']['URL']
        response = urllib2.urlopen(imagepageurl)
        html = response.read()
        root = lxml.html.fromstring(html)
        url = root.find_class('image')[0].attrib.get('src')
        return url
        
    def isInitialized(self):
        return self.keys['API_KEY'] != None \
            and self.keys['API_SECRET'] != None \
            and self.keys['OAUTH_TOKEN'] != None \
            and self.keys['OAUTH_TOKEN_SECRET'] != None
            
    request_unauth_token_url = 'http://www.imagebam.com/sys/oauth/request_token'
    verifier_url = 'http://www.imagebam.com/sys/oauth/authorize_token'
    request_token_url = 'http://www.imagebam.com/sys/oauth/request_access_token'
    post_image_url = 'http://www.imagebam.com/sys/API/resource/upload_image'
    
