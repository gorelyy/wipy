import subprocess

def gitAdd(gitpath, filename):
    cmd = ['git', 'add', filename]
    p = subprocess.Popen(cmd, cwd=gitpath)
    p.wait()


def gitInit(gitpath):
    cmd = ['git', 'init']
    p = subprocess.Popen(cmd, cwd=gitpath)
    p.wait()


def gitCreateBranch(gitpath, branchname):
    cmd = ['git', 'checkout', '-b', branchname]
    p = subprocess.Popen(cmd, cwd=gitpath)
    p.wait()


def gitAddRemote(gitpath, name, url):
    cmd = ['git', 'remote', 'add', name, url]
    p = subprocess.Popen(cmd, cwd=gitpath)
    p.wait()


def gitCommit(gitpath, message):
    cmd = ['git', 'commit', '-a', '-m', message]
    p = subprocess.Popen(cmd, cwd=gitpath)
    p.wait()


def gitPush(gitpath, remote, localbranch):
    cmd = ['git', 'push', remote, localbranch]
    p = subprocess.Popen(cmd, cwd=gitpath)
    p.wait()