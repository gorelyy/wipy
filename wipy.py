# -*- coding: UTF-8 -*-
import os
import sys
import yaml
import ctypes
import subprocess
import lxml.html
import shutil
import datetime

import wipygit
from wipyimagebam import Imagebam
from wipypage import Page
from wipytag import Tag


class Wipy:

    def __init__(self):
        self.installpath = os.path.dirname(os.path.realpath(sys.argv[0]))
        projectsfile = open(self.installpath + '/projects.yml', 'r')
        self.projects = yaml.load(projectsfile)
        projectsfile.close()
        self.path = self.projects['current']
        if self.path and os.path.exists(self.path):
            self.mainconfig = {}
            self.readConfig()

    def readConfig(self):
        config = open(self.path + '/config.yml', 'r')
        self.mainconfig = yaml.load(config)

    def writeConfig(self):
        config = open(self.path + '/config.yml', 'w')
        config.write(yaml.dump(self.mainconfig, default_flow_style=False))

    def processCommand(self):
        if len(sys.argv) == 1:
            print(u"Команда не была указана")
            return

        value = ''
        command = sys.argv[1]

        for i in range(2, len(sys.argv)):
            value = value + ' ' + sys.argv[i]
        value = value.strip()

        if command == 'init':
            self.runInit(value)
            return
        if command == 'edit':
            self.runEdit(value)
            return
        if command == 'delete':
            self.runDelete(value)
            return
        if command == 'rename':
            self.runRename(value)
            return
        if command == 'publish':
            self.runPublish()
            return
        if command == 'list':
            self.runList()
            return
        if command == 'browse':
            self.runBrowser(value)
            return
        if command == 'switch':
            self.runSwitchProject(value)
            return
        if command == 'update':
            self.runUpdate(value)
            return
        if command == 'deleteproject':
            self.runDeleteProject(value)
            return
        if command == 'nav':
            self.runNavigate(value)
            return
        if command == 'config':
            self.runConfig()
            return
        if command == 'server':
            self.runStartServer()
            return
        if command == 'killserver':
            self.runKillServer()
            return
            
        print u'Неподдерживаемая команда "' + command + u'"'

    def projectExists(self):
        if self.path and os.path.exists(self.path):
            return True
        else:
            if not self.projects.keys():
                print u"Проекты не найдены, используйте команду 'wipy init <project name>' для создания проекта"
            else:
                print u"Ни один из проектов не является открытым, используйте команду 'wipy switch <project name>' или создайте новый проект"
            return False

    def runSwitchProject(self, name):
        if not name:
             print u"Имя проекта не укзаано"
             return
        if name not in self.projects['all'].keys():
            print u"Проект'" + name + u"' не существует"
            return
        self.projects['current'] = self.projects['all'][name]
        projectsfile = open(self.installpath + '/projects.yml', 'w')
        projectsfile.write(yaml.dump(self.projects, default_flow_style=False))
        projectsfile.close()

    def runInit(self, projectname):
        def is_subdir(path, directory):
            path = os.path.realpath(path)
            directory = os.path.realpath(directory)
            if path[0] != directory[0]: #drive letter
                return False
            relative = os.path.relpath(path, directory)
            if relative.startswith(os.pardir + os.sep):
                return False
            else:
                return True
        path = os.getcwd()
        path = path.replace('\\','/')
        if not path[-1:] == '/':
            path = path + '/'
        path = path + projectname
        self.path = path
        
        if not projectname:
            print u'No name has been specified'
            return
        projectsfile = open(self.installpath + '/projects.yml', 'r')
        projects = yaml.load(projectsfile)
        projectsfile.close()
        
        if projectname in projects['all'].keys():
            print u'Проект "' + projectname + u'" уже существует'
            return
        
        for key, val in projects['all'].iteritems():
            if is_subdir(path, val):
                print u'Невозможно создать проект в директории проекта "' + key + u'"'
                return
                
        if not os.path.exists(path):
            if '#' in projectname or '.' in projectname:
                print u'Имя ' + projectname + u' содержит неподдерживаемые символы. Создание проекта остановлено'
                return
            try:
                os.makedirs(path)
            except WindowsError:
                print u'Имя ' + projectname + u' содержит неподдерживаемые символы. Создание проекта остановлено'
                return       
            os.makedirs(path + '/tag')
            os.makedirs(path + '/template')
            os.makedirs(path + '/style')
            os.makedirs(path + '/files')
            os.makedirs(path + '/imagebam')
            open(path + '/imagebam/urllist.yml', 'w').close()
            shutil.copyfile(self.installpath + '/page.html', path + '/template/page.html')
            shutil.copyfile(self.installpath + '/tagpage.html', path + '/template/tagpage.html')
            shutil.copyfile(self.installpath + '/default.css', path + '/style/default.css')
            shutil.copyfile(self.installpath + '/pygments.css', path + '/style/pygments.css')
            shutil.copyfile(self.installpath + '/projectconfig.yml', path + '/config.yml')
            shutil.copyfile(self.installpath + '/imagebamkeys.yml', path + '/imagebamkeys.yml')
            templatefile = open(self.installpath + '/page.html', 'r')
            template = templatefile.read()
            templatefile.close()
            root = lxml.html.fromstring(template)       
            header = lxml.html.fromstring('<h1>homepage</h1>')
            pageheader = root.find_class('pageheader')
            pageheader[0].insert(0, header)
            mainpage = open(path + '/index.html', 'w')
            mainpage.write(lxml.html.tostring(root))
            mainpage.close()
            
            self.readConfig()
            Page(self.path, self.mainconfig['homepage'], self.mainconfig, addtogit=False)
             
            wipygit.gitInit(path)
            wipygit.gitCreateBranch(path, 'gh-pages')
            wipygit.gitAdd(path, 'index.html')
            
        projects['all'][projectname] = path
        projects['current'] = path
        projectsfile = open(self.installpath + '/projects.yml', 'w')
        projectsfile.write(yaml.dump(projects,default_flow_style=False))
        projectsfile.close()

    def runEdit(self, pagename):
        if not self.projectExists():
            return
        if not self.mainconfig['editor']:
            print u'Текстовый редактор не задан. Используйте команду wipy config'
            return
        if not os.path.exists(self.path + '/' + pagename.replace(' ', '_')):
            choice = raw_input(r'Страница не существует, создать? Y/N')
            if not (choice == 'Y' or choice == 'y'):
                return

        page = Page(self.path, pagename, self.mainconfig)
        if not page.valid:
            return
        subprocess.call(
            [self.mainconfig['editor'], page.path + '/page.md'], shell=True)
        self.uploadImagebam()
        page.parseMarkdown()
        page.generateHTML()
        if pagename == self.mainconfig['homepage']:
            shutil.copyfile(page.path + '/index.html',
                            self.path + '/index.html')

    def runDelete(self, pagename):
        if not self.projectExists():
            return
        if not os.path.exists(self.path + '/' + pagename.replace(' ', '_')):
            print u'Страница не существует'
            return
        page = Page(self.path, pagename, self.mainconfig)
        page.delete()

    def runRename(self, arg):
        if not self.projectExists():
            return
        arg = arg.split('/')
        pagename = arg[0].strip()
        newpagename = arg[1].strip()
        if not os.path.exists(self.path + '/' + pagename.replace(' ', '_')):
            return
        if os.path.exists(self.path + '/' + newpagename.replace(' ', '_')):
            return
        page = Page(self.path, pagename, self.mainconfig)
        page.rename(newpagename)

    def runList(self):
        if not self.projectExists():
            return        
        print u'Открытый проект:"' + self.projects['current'].rsplit('/',1)[1] + '"'
        print u'Список проектов:'
        for key, val in self.projects['all'].iteritems():
            print '"'+key+'" (' + val + ')' 

    def runBrowser(self, pagename):
        if not self.projectExists():
            return
        if not self.mainconfig['browser']:
            print u'Веб браузер не задан. Используйте команду wipy config'
        os.chdir(self.path)
        p = None
        if not self.mainconfig['serverpid']:
            p = subprocess.Popen(['python', '-m', 'SimpleHTTPServer', str(self.mainconfig['port'])])
        subprocess.call(
            [self.mainconfig['browser'], self.mainconfig['browserurlarg'] or '', 'localhost:' + str(self.mainconfig['port']) + '/' + pagename])
        if p:
            p.kill()
    
    def runStartServer(self):
        if not self.projectExists():
            return
        os.chdir(self.path)
        p = subprocess.Popen(['python', '-m', 'SimpleHTTPServer', str(self.mainconfig['port'])], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.mainconfig['serverpid'] = p.pid
        self.writeConfig()
    
    def runKillServer(self):
        if not self.mainconfig['serverpid']:
            print u'Работающий сервер не был найден'
            return
        PROCESS_TERMINATE = 1
        handle = ctypes.windll.kernel32.OpenProcess(PROCESS_TERMINATE, False, int(self.mainconfig['serverpid']))
        ctypes.windll.kernel32.TerminateProcess(handle, -1)
        ctypes.windll.kernel32.CloseHandle(handle)
        self.mainconfig['serverpid'] = None
        self.writeConfig()
        
    def runUpdate(self, pagename):
        if not self.projectExists():
            return
        if pagename != '':
            page = Page(self.path, pagename, self.mainconfig)
            page.parseMarkdown()
            page.generateHTML()
            if pagename == self.mainconfig['homepage']:
                shutil.copyfile(page.path + '/index.html',
                                self.path + '/index.html')
            return
        for name in os.listdir(self.path):
            if os.path.isdir(self.path + "/" + name) \
                    and name not in ['tag', '.git', 'files', 'imagebam', 'style', 'template']:
                page = Page(self.path, name, self.mainconfig)
                page.parseMarkdown()
                page.generateHTML()

        shutil.copyfile(self.path + '/' + self.mainconfig['homepage'] + '/index.html',
                        self.path + '/index.html')

    def runPublish(self):
        if not self.projectExists():
            return
        if not self.mainconfig['gitremote']:
            print u'Удаленный git не был задан. Используйте команду wipy config'
            return
        self.uploadFiles()
        wipygit.gitCommit(self.path, str(datetime.datetime.now()))
        wipygit.gitPush(self.path, self.mainconfig['gitremote'], 'gh-pages')
        
    def uploadFiles(self):
        for root, dirs, files in os.walk(self.path + '/files'):
            for file in files:
                filepath = os.path.join(root,file)
                filename = filepath.replace(self.path + '/','').replace('\\', '/')
                wipygit.gitAdd(self.path, filename)
        for root, dirs, files in os.walk(self.path + '/style'):
            for file in files:
                filepath = os.path.join(root,file)
                filename = filepath.replace(self.path + '/','').replace('\\', '/')
                miogit.gitAdd(self.path, filename)
        
    def uploadImagebam(self):
        urllistfile = open(self.path + '/imagebam/urllist.yml', 'r')
        urllist = yaml.load(urllistfile)
        urllistfile.close()
        if not urllist:
            urllist = {}
        newimages = []
        for root, dirs, files in os.walk(self.path + '/imagebam'):
            for file in files:
                if file.endswith('.jpg') or file.endswith('.png') or file.endswith('.gif'):
                    filepath = os.path.join(root,file)
                    filename = filepath.replace(self.path + '/imagebam\\','').replace('\\', '/')
                    if filename not in urllist.keys():
                        newimages.append((filename, filepath))
        if len(newimages) > 0:
            ib = Imagebam(self.path + '/imagebamkeys.yml')
            if not ib.initialized:
                return
            for image in newimages:
                urllist[image[0]] = ib.uploadImage(image[1])
            urllistfile = open(self.path + '/imagebam/urllist.yml', 'w')
            urllistfile.write(yaml.dump(urllist, default_flow_style=False))
            
    def runDeleteProject(self, name):
        if not name:
             print(u"Имя проекта не указано")
             return
        if name not in self.projects['all'].keys():
            print u"Проект '" + name + u"' не существует"
            return
        
        def onerror(func, path, exc_info):
            """
            Error handler for ``shutil.rmtree``.
            If the error is due to an access error (read only file)
            it attempts to add write permission and then retries.
            If the error is for another reason it re-raises the error.
            Usage : ``shutil.rmtree(path, onerror=onerror)``
            """
            import stat
            if not os.access(path, os.W_OK):
                # Is the error an access error ?
                os.chmod(path, stat.S_IWUSR)
                func(path)
            else:
                raise
                       
        shutil.rmtree(self.projects['all'][name], onerror=onerror)
        if self.projects['current'] == self.projects['all'][name]:
            self.projects['current'] = None
        del self.projects['all'][name]
        projectsfile = open(self.installpath + '/projects.yml', 'w')
        projectsfile.write(yaml.dump(self.projects, default_flow_style=False))

    def runNavigate(self, folder):
        if not self.projectExists():
            return
        if not folder in ['files', 'imagebam', 'style', 'template']:
            print u'Неизвестная директория ' + folder
            return
        folder = (self.path + '/' + folder).replace('/', '\\')
        subprocess.Popen('explorer /cwd="' + folder + '"')
    
    def runConfig(self):
        if not self.projectExists():
            return
        if not self.mainconfig['editor']:
            print u'Текстовый редактор не был задан. Используйте команду wipy config'
            return
        subprocess.call([self.mainconfig['editor'], self.path + '/config.yml'])
    
wipy = Wipy()
wipy.processCommand()
