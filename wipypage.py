# -*- coding: UTF-8 -*-
import os
import sys
import yaml
import re
import shutil
import lxml.html
import markdown2
import wipygit
from wipytag import Tag

class Page:
    def __init__(self, root, pagename, mainconfig, addtogit = True):
        self.installpath = os.path.dirname(os.path.realpath(sys.argv[0]))
        self.mainconfig = mainconfig
        self.config = {}
        self.mdparts = {}
        self.linkedpages_ = set()
        self.root = root
        self.name = pagename
        self.idname = pagename.replace(' ', '_')
        self.path = root + '/' + self.idname
        self.valid = False
        if not os.path.exists(self.path):
            if '#' in self.name or '.' in self.name:
                print u'Имя ' + self.name + u' содержит неподдерживаемые символы. Создание страницы остановлено'
                return
            try:
                os.makedirs(self.path)
            except WindowsError:
                print u'Имя ' + self.name + u' содержит неподдерживаемые символы. Создание страницы остановлено'
                return
            open(self.path + '/page.md', 'w').close()
            
            #create page config and set a name
            configfile = open(self.installpath + '/pageconfig.yml', 'r');
            pageconfig = yaml.load(configfile)
            configfile.close()
            pageconfig['fullname'] = self.name
            configfile = open(self.path + '/config.yml', 'w')
            configfile.write(yaml.dump(pageconfig,default_flow_style=False))
            configfile.close()
            
             #create html with page name as title and header
            template = open(self.root + '/template/page.html', 'r').read()
            root = lxml.html.fromstring(template)
            header = lxml.html.fromstring(unicode('<h1>'+self.name+'</h1>', 'utf-8'))
            pageheader = root.find_class('pageheader')
            pageheader[0].insert(0, header)
            for title in root.iter('title'):
                title.text = self.mainconfig['title']['front'] + unicode(self.name,'utf-8') \
                    + self.mainconfig['title']['back']
            index = open(self.path + '/index.html', 'w')
            index.write(lxml.html.tostring(root))
            if addtogit:
                wipygit.gitAdd(self.root, self.idname + '/index.html')
        self.readConfig()
        self.name = self.config['fullname']
        self.valid = True

    def readConfig(self):
        config = open(self.path + '/config.yml', 'r')
        self.config = yaml.load(config)
        config.close()

    def writeConfig(self):
        stream = open(self.path + '/config.yml', 'w')
        stream.write(yaml.dump(self.config, default_flow_style=False))

    # Creates links, anchors, images
    def processKeyword(self, match):
        content = [each.strip() for each in match.group()[2:-2].split('|')]
        link = content[0].strip()
        name = link
        if len(content) > 1:
            name = content[1].strip()
        if link.startswith('img:') or link.startswith('img<'):
            return self.processImageLink(link, name)
        if link.startswith('imgbam:') or link.startswith('imgbam<'):
            return self.processImagebamLink(link, name)            
        if link.startswith('section:'):
            link = link[8:]
            return '<a name = "' + link + '"></a>'
        anchor = ''
        if '#' in link:
            #link to tag page
            if link.startswith('#'):
                link = '/tag/' + link[1:]
                return '<a href="' + link.replace(' ','_') + '">' + name + '</a>' 
            #link to section of page 
            else:
                linksplit = link.split('#')
                link = linksplit[0]
                anchor = linksplit[1]                
        self.linkedpages_.add(link)
        if link not in self.config['linksto']:
            self.config['linksto'].append(link)
            self.writeConfig()
            linkpage = Page(self.root, link, self.mainconfig)
            linkpage.config['linkedfrom'].append(self.name)
            linkpage.writeConfig()
        if link == self.mainconfig['homepage']:
            link = ''
        if anchor:
            anchor = '#' + anchor
        return '<a href="/' + link.replace(' ','_') + anchor + '">' + name + '</a>' 
        #return '[' + name + '](/' + link.replace(' ', '_') + ')'
    
    def processImageLink(self, link, name):
        linksplit = link.rsplit(':',1)
        link = linksplit[1]
        attr = linksplit[0]
        if attr.startswith('img<'):
            attr = attr.split('<')[1]
            attr = attr[:-1].strip()
        else:
            attr = self.mainconfig['imageattribute']
        if name.startswith('img'):
            name = link
        anchor = ''
        tagurl = False
        if '.' not in name:  # page name link, not url
            if '#' in name:
                if name.startswith('#'):
                    name = '/tag/' + name[1:].replace(' ', '_')
                    tagurl = True  
                else:
                    namesplit = name.split('#')
                    name = namesplit[0]
                    anchor = namesplit[1]                  
            if not tagurl:
                self.linkedpages_.add(name)
                if name not in self.config['linksto']:
                    self.config['linksto'].append(name)
                    self.writeConfig()
                    linkpage = Page(self.root, name, self.mainconfig)
                    linkpage.config['linkedfrom'].append(self.name)
                    linkpage.writeConfig()
                name = '/' + name.replace(' ', '_')
                if name == '/' + self.mainconfig['homepage']:
                    name = '/'                   
        nameislink = False
        if link == name:
            nameislink = True
        link = link.replace('\\', '/')
        if link.startswith('/'):
            link = link[1:] 
        if nameislink:
            name = '/files/' + link
        if not name.startswith('/'): #if name is url
            name = '//' + name
        name = name.replace('http://', '//')
        name = name.replace('https://', '//')
        if anchor:
            anchor = '#' + anchor 
        return '<a href="' + name + anchor + '"><img ' + attr + ' src="/files/' + link + '"></a>'
    
    def processImagebamLink(self, link, name):
        linksplit = link.rsplit(':',1)
        link = linksplit[1]
        attr = linksplit[0]
        if attr.startswith('imgbam<'):
            attr=attr.split('<')[1]
            attr = attr[:-1].strip()
        else:
            attr = self.mainconfig['imageattribute']
        if name.startswith('imgbam'):
            name = link
        anchor = ''
        tagurl = False
        if '.' not in name:  # page name link, not url
            if '#' in name:
                if name.startswith('#'):
                    name = '/tag/' + name[1:].replace(' ', '_')
                    tagurl = True    
                else:
                    namesplit = name.split('#')
                    name = namesplit[0]
                    anchor = namesplit[1]                  
            if not tagurl:
                self.linkedpages_.add(name)
                if name not in self.config['linksto']:
                    self.config['linksto'].append(name)
                    self.writeConfig()
                    linkpage = Page(self.root, name, self.mainconfig)
                    linkpage.config['linkedfrom'].append(self.name)
                    linkpage.writeConfig()
                name = '/' + name.replace(' ', '_')
                if name == '/' + self.mainconfig['homepage']:
                    name = '/'          
        nameislink = False
        if link == name:
            nameislink = True
        link = link.replace('\\', '/')
        if link.startswith('/'):
            link = link[1:]    
        urllistfile = open(self.root + '/imagebam/urllist.yml', 'r')
        urllist = yaml.load(urllistfile)
        urllistfile.close()
        imageurl = ''
        if not urllist or link not in urllist.keys():
            print u'Ошибка: загрузка изображений не была подтверждена на сайте Imagebam или файл ' + link + u' не существует'
            link = '#'
        else:
            imageurl = urllist[link]
        if nameislink:
            name = imageurl
        if not name.startswith('/'):
            name = '//' + name
        name = name.replace('http://', '//')
        name = name.replace('https://', '//')
        if anchor:
            anchor = '#' + anchor 
        return '<a href="' + name + anchor + '"><img ' + attr + ' src="' + imageurl + '"></a>'
    
    def parseMarkdown(self):
        page = open(self.path+'/page.md', 'r')
        text = page.read()
        page.close()

        # [[keyword]]
        #p = re.compile(r'\[\[ *?\w.*?\]\]')
        p = re.compile(r'\[\[.*?\]\]')
        text = p.sub(self.processKeyword, text)
        self.updateLinksAfterParsing()
        
        # !///keyword
        p = re.compile(r'(!\/\/\/\w+.*\n?)')
        splitresult = p.split(text)

        for i in range(1, len(splitresult)-1, 2):
            divclass = splitresult[i].strip().lstrip('!///')
            self.mdparts[divclass] = splitresult[i+1]
        
        # Преобразовывает теги в ссылки
        if 'tags' in self.mdparts:
            taglist = map(str.strip, self.mdparts['tags'].split(','))
            taglist = filter(None, taglist)
            tagset = set(taglist)
            urltags = ''
            i = 0
            for tag in tagset:
                i = i + 1
                if tag != '':
                    separator = ', '
                    if i == len(tagset):
                        separator = ''
                    self.processTag(tag.strip())
                    urltags = urltags + \
                        '[#' + tag + '](/tag/'+tag.replace(' ','_')+')' + separator
            self.mdparts['tags'] = urltags
            if len(taglist) < len(self.config['taggedwith']):
                # remove missing tags from config, remove page from tag,
                # check if tag has other pages, if not - remove tag
                missingtags = set(self.config['taggedwith']) - set(taglist)
                for tagname in missingtags:
                    self.config['taggedwith'].remove(tagname)
                    tag = Tag(self.root, tagname, self.mainconfig)
                    if tag.pageCount() <= 1:
                        tag.delete()
                    else:
                        tag.removePage(self.name)
                self.writeConfig()
        else:
            missingtags = set(self.config['taggedwith'])
            for tagname in missingtags:
                self.config['taggedwith'].remove(tagname)
                tag = Tag(self.root, tagname, self.mainconfig)
                if tag.pageCount() <= 1:
                    tag.delete()
                else:
                    tag.removePage(self.name)
            self.writeConfig()
        return

    def generateHTML(self):
        # create page directory for link if not existed
        for pagename in self.linkedpages_:
            Page(self.root, pagename, self.mainconfig)
        #
        
        headername = self.name
        titlename = self.name
        template = 'page.html'
        style = 'default.css'
        
        for div, md in self.mdparts.iteritems():
            if div == 'header':
                headername = md.splitlines()[0].strip()
                continue
            if div == 'title':
                titlename = md.splitlines()[0].strip()
                continue
            if div == 'template':
                template = md.splitlines()[0].strip()
                continue
            if div == 'style':
                style = md.splitlines()[0].strip()
                continue
                
        template = open(self.root + '/template/' + template, 'r').read()
        root = lxml.html.fromstring(template)        
        link = root.find('.//link')
        link.drop_tree()
        head = root.find('.//head')
        stylesheet = lxml.html.fromstring('<link rel="stylesheet" href="/style/pygments.css">').find('.//link')
        head.insert(2, stylesheet)
        stylesheet = lxml.html.fromstring('<link rel="stylesheet" href="/style/' + style + '">').find('.//link')
        head.insert(2, stylesheet)
        for div, md in self.mdparts.iteritems():
            if div == 'header' or div == 'title' or div == 'template' or div == 'style':
                continue
            chunk = markdown2.markdown(
                #md, extras={"fenced-code-blocks":None, "html-classes": {"img": "pure-img"}})
                md, extras = ["fenced-code-blocks"])
            newhtml = lxml.html.fromstring(chunk)
            target = root.find_class(div)
            if not target:
                continue
            target[0].insert(0, newhtml)
        header = lxml.html.fromstring(unicode('<h1>'+headername+'</h1>', 'utf-8'))
        pageheader = root.find_class('pageheader')
        pageheader[0].insert(0, header)
        for title in root.iter('title'):
            title.text = self.mainconfig['title']['front'] + unicode(titlename, 'utf-8') \
                + self.mainconfig['title']['back']
        index = open(self.path + '/index.html', 'w')
        index.write(lxml.html.tostring(root))
        index.close()

    def processTag(self, tagname):
        if tagname in self.config['taggedwith']:
            return
        tag = Tag(self.root, tagname, self.mainconfig)
        if not tag.valid:
            return
        tag.addPage(self.name)
        self.config['taggedwith'].append(tagname)
        self.writeConfig()
    
    # Работает с разницей между сохраненными ссылками и ссылками, присутствующими
    # в Markdown файле после внесения изменений, поэтому должна вызываться после обработки
    # Markdown файла, иначе (например при вызове сразу после создания объекта Page),
    # удалит информацию о всех ссылках находящихся в тексте, это поведение используется 
    # в методе удаления страницы  
    def updateLinksAfterParsing(self):
        oldlinks = set(self.config['linksto']) - self.linkedpages_
        for link in oldlinks:
            self.config['linksto'].remove(link)
            linkpage = Page(self.root, link, self.mainconfig)
            linkpage.config['linkedfrom'].remove(self.name)
            linkpage.writeConfig()
        self.writeConfig()

    def delete(self):
        # replace links to text in linked from
        for link in self.config['linkedfrom']:
            page = Page(self.root, link, self.mainconfig)
            page.deleteLinks(self.name)
            page.parseMarkdown()
            page.generateHTML()
        # remove from tag page
        for tagname in self.config['taggedwith']:
            tag = Tag(self.root, tagname, self.mainconfig)
            tag.removePage(self.name)
        self.linkedpages_.clear()
        self.updateLinksAfterParsing()
        shutil.rmtree(self.path)

    def removeMarkdownURL(self, match):
        content = [each.strip() for each in match.group()[2:-2].split('|')]
        if content[0].startswith('img:'):
            if len(content) > 1 and content[1] == self.deletedlink:
                return '[[' + content[0] + ']]'
        if content[0].startswith('imgbam:'):
            if len(content) > 1 and content[1] == self.deletedlink:
                return '[[' + content[0] + ']]'
        if content[0] == self.deletedlink:
            if len(content) == 1:
                return content[0]
            return content[1]
        return match.group()

    def deleteLinks(self, link):
        page = open(self.path+'/page.md', 'r')
        text = page.read()
        page.close()
        self.deletedlink = link
        p = re.compile(r'\[\[ *?\w.*?\]\]')
        text = p.sub(self.removeMarkdownURL, text)
        page = open(self.path+'//page.md', 'w')
        page.write(text)
        page.close()

    def renameMarkdownURL(self, match):
        content = [each.strip() for each in match.group()[2:-2].split('|')]
        if content[0].startswith('img:'):
            if len(content) > 1 and content[1] == self.oldlink:
                return '[[' + content[0] + '|' + self.newlink + ']]'
        if content[0].startswith('imgbam:'):
            if len(content) > 1 and content[1] == self.oldlink:
                return '[[' + content[0] + '|' + self.newlink + ']]'
        if content[0] == self.oldlink:
            if len(content) == 1:
                return '[[' + self.newlink + ']]'
            return '[[' + self.newlink + '|' + content[1] + ']]'
        return match.group()

    def renameLinks(self, link, newlink):
        page = open(self.path+'/page.md', 'r')
        text = page.read()
        page.close()
        self.oldlink = link
        self.newlink = newlink
        p = re.compile(r'\[\[ *?\w.*?\]\]')
        text = p.sub(self.renameMarkdownURL, text)
        page = open(self.path+'/page.md', 'w')
        page.write(text)
        page.close()

    def rename(self, newname):
        prevname = self.name
        self.name = newname
        self.idname = newname.replace(' ', '_')
        newpath = self.root + '/' + self.idname
        os.rename(self.path, newpath)
        self.path = newpath
        self.readConfig()
        self.config['fullname'] = newname
        self.writeConfig()
        for link in self.config['linksto']:
            page = Page(self.root, link, self.mainconfig)
            page.config['linkedfrom'].remove(prevname)
            page.config['linkedfrom'].append(newname)
            page.writeConfig()
        for link in self.config['linkedfrom']:
            page = Page(self.root, link, self.mainconfig)
            page.config['linksto'].remove(prevname)
            page.config['linksto'].append(newname)
            page.writeConfig()
            page.renameLinks(prevname, newname)
            page.parseMarkdown()
            page.generateHTML()
        self.parseMarkdown()
        self.generateHTML()

        for tagname in self.config['taggedwith']:
            tag = Tag(self.root, tagname, self.mainconfig)
            tag.renamePage(prevname, newname)